import React from 'react';
import moment from 'moment';

interface DataShape {
    startDate: string;
    endDate: string;
};

export default function SleepChart(props: {
    data: DataShape,
    // To supply differnt victory library for the different environment: DOM and native
    victory: { VictoryBar
        , VictoryChart, VictoryAxis, VictoryTooltip, VictoryLabel, VictoryCandlestick },
    platform: 'web' | 'ios' | 'android'
}) {
    const { data } = props;
    const { VictoryBar, VictoryChart, VictoryAxis, VictoryTooltip, VictoryLabel, VictoryCandlestick } = props.victory;
    
    const parsedData = helper.parseHKSleepData(data);
    console.log("Does it reach here?");

    const victoriousData = parsedData.filter(sleep => {
      return sleep.date.month === 5;
    }).map(sleep => {
      return {
        x: new Date(sleep.date.year, sleep.date.month, sleep.date.date),
        open: helper.getY(sleep.start.hour, sleep.start.minute),
        close: helper.getY(sleep.end.hour, sleep.end.minute),
        high: 0,
        low: 0,
        label: `To bed: ${helper.zeroPad(sleep.start.hour, 2)}:${helper.zeroPad(sleep.start.minute, 2)}\n Wake up: ${helper.zeroPad(sleep.end.hour, 2)}:${helper.zeroPad(sleep.end.minute,2)}`
      };
    });
    
    const styles = helper.getStyles();
    const tickValues = helper.getTickValues();
    const rangeY = [-1000, 50];

    return (
        <VictoryChart>
            <VictoryLabel 
            x={25} 
            y={24} 
            style={styles.title}
            text="Sleep Data"
            />

            <VictoryLabel x={430} y={20} style={styles.labelNumber}
            text="1"
            />

            <VictoryAxis
                scale="time"
                offsetX={50}
                standalone={false}
                style={styles.axisYears}
                tickValues={tickValues}
            /> 

            <VictoryAxis dependentAxis
                domain={[12, 30]}
                offsetX={50}
                orientation="left"
                standalone={false}
                style={styles.axisOne}
                tickValues={[27, 24, 21, 18, 15, 12]}
                tickFormat={["21:00", "00:00", "3:00", "6:00", "9:00", "12:00"]}
            />

            <VictoryCandlestick
                domain={{
                    x: [new Date(2017, 5, 13), new Date(2017, 5, 30)],
                    y: [12, 30]
                }}
                data = { victoriousData }
                cornerRadius={25}
                style={{
                    data: { 
                    fill: '#3F8DFF', stroke: '#3F8DFF'
                    }
                }}
                labelComponent={<VictoryTooltip dy={300}/>}
            />
        </VictoryChart>
    );
}

const helper = {
  getY: (hour, minute) => {
    if (hour < 12) {
      const delta = 12 - (hour + ((minute / 60) * 1) ) ;
      return 12 + delta
    } else {
      const delta = 24 - (hour + ((minute / 60) * 1) );
      return 24 + delta
    }
  },
  parseHKSleepData: (sleepData) => {
    return sleepData.map( s => {
      const startDate = moment(s.startDate); 
      const endDate = moment(s.endDate);
      const anchor = (startDate.hour() < 12) ? startDate.subtract(1, 'days') : startDate;

      return {
        date: {
          year: anchor.year(), 
          month: anchor.month(), 
          date: anchor.date()
        },
        start: {
          hour: startDate.hour(),
          minute: startDate.minute()
        },
        end: {
          hour: endDate.hour(),
          minute: endDate.minute()
        }
      };
    });
  },
  zeroPad: (num, places) => {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  },
  getTickValues:() => {
    return [
      new Date(2017, 5, 13),
      new Date(2017, 5, 20),
      new Date(2017, 5, 25),
      new Date(2017, 5, 30),
    ];
  },
  getStyles:() => {
    const BLUE_COLOR = "#00a3de";
    const RED_COLOR = "#ff0000";

    return {

      title: {
        textAnchor: "start",
        verticalAnchor: "end",
        fill: "#ffffff",
        fontFamily: "inherit",
        fontSize: "18px",
        fontWeight: "bold"
      },
      labelNumber: {
        textAnchor: "middle",
        fill: "#ffffff",
        fontFamily: "inherit",
        fontSize: "14px"
      },

      // INDEPENDENT AXIS
      axisYears: {
        axis: { stroke: "black", strokeWidth: 1},
        ticks: {
          size: (tick) => {
            const tickSize =
              tick.getFullYear() % 5 === 0 ? 10 : 5;
            return tickSize;
          },
          stroke: "#ffffff",
          strokeWidth: 1
        },
        tickLabels: {
          fill: "#ffffff",
          fontFamily: "inherit",
          fontSize: 16
        }
      },

      // DATA SET ONE
      axisOne: {
        grid: {
          stroke: (tick) =>
            tick === -10 ? "transparent" : "#ffffff",
          strokeWidth: 2
        },
        axis: { stroke: "#ffffff", strokeWidth: 0 },
        ticks: { strokeWidth: 0 },
        tickLabels: {
          fill: '#ffffff',
          fontFamily: "inherit",
          fontSize: 16
        }
      },
      labelOne: {
        fill: BLUE_COLOR,
        fontFamily: "inherit",
        fontSize: 12,
        fontStyle: "italic"
      },
      lineOne: {
        data: { stroke: BLUE_COLOR, strokeWidth: 4.5 }
      },
      axisOneCustomLabel: {
        fill: BLUE_COLOR,
        fontFamily: "inherit",
        fontWeight: 300,
        fontSize: 21
      },

      // DATA SET TWO
      axisTwo: {
        axis: { stroke: RED_COLOR, strokeWidth: 0 },
        tickLabels: {
          fill: RED_COLOR,
          fontFamily: "inherit",
          fontSize: 16
        }
      },
      labelTwo: {
        textAnchor: "end",
        fill: RED_COLOR,
        fontFamily: "inherit",
        fontSize: 12,
        fontStyle: "italic"
      },
      lineTwo: {
        data: { stroke: RED_COLOR, strokeWidth: 4.5 }
      },

      // HORIZONTAL LINE
      lineThree: {
        data: { stroke: "#e95f46", strokeWidth: 2 }
      }
    };
  }
}