import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import sleepData from './data/sleep'
import * as victory from 'victory';
import SleepChart from './SleepChart';

export class TatsuyaChart extends React.Component<{}, { data: DataShape }> {

  constructor(props) {
    super(props);
    this.state = { data: sleepData }
  }

  render() {
    return <SleepChart data={this.state.data} victory={victory} platform={'web'} />;
  }
}



