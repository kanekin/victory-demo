import React from 'react';
import sleepData from './data/sleep';
import * as victory from 'victory';
import SleepChart from './SleepChart';
export class TatsuyaChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: sleepData };
    }
    render() {
        return React.createElement(SleepChart, { data: this.state.data, victory: victory, platform: 'web' });
    }
}
//# sourceMappingURL=tatsuya_chart.js.map