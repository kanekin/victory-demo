import React from 'react';
import moment from 'moment';

interface DataShape {
    startDate: number;
    endDate: number;
    value: {
        startDate: number;
        value: number;
        endDate: number;

    }[];
};

export default function Chart(props: {
    data: DataShape,
    // To supply differnt victory library for the different environment: DOM and native
    victory: { VictoryBar, VictoryChart, VictoryAxis, VictoryTooltip, VictoryVoronoiContainer },
    platform: 'web' | 'ios' | 'android'
}) {
    const { data } = props;
    const { VictoryBar, VictoryChart, VictoryAxis, VictoryTooltip, VictoryVoronoiContainer } = props.victory;
    const processedData = data.value.map(value => {
        return {
            x: new Date(value.startDate).getTime(),
            y: value.value,
            label: `${moment(value.startDate).format('ddd, Do MMM')}\n${Math.floor(value.value)} steps`
        }
    });
    const tickValues = processedData.filter(datum => moment(datum.x).day() === 0).map(datum => datum.x);
    return <VictoryChart domainPadding={10}
                         containerComponent={<VictoryVoronoiContainer dimension="x" />} >
        <VictoryAxis tickFormat={date => moment(date).format('l')}
                     tickValues={tickValues}
                     style={{ grid: { stroke: 'white' } }} />
        <VictoryAxis dependentAxis={true}
                     style={{ grid: { stroke: 'white' } }} />
        <VictoryBar data={processedData}
                    labelComponent={<VictoryTooltip/>} />
    </VictoryChart>;
}