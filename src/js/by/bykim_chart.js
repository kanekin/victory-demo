import React from 'react';
import dailyData from './data/30days-by-day.js';
import Chart from './Chart';
import * as victory from 'victory';
export class BYKimChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: dailyData };
    }
    render() {
        return React.createElement(Chart, { data: this.state.data, victory: victory, platform: 'web' });
    }
}
//# sourceMappingURL=bykim_chart.js.map