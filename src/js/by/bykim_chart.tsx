import React from 'react';
import ReactDOM from 'react-dom';
import dailyData from './data/30days-by-day.js';
import weeklyData from './data/30days-by-week.js';
import hourlyData from './data/30days-by-hour.js';
import tenMinutelyData from './data/30days-by-ten-minutes.js';
import Chart from './Chart';

import * as victory from 'victory';

export class BYKimChart extends React.Component<{}, { data: DataShape }> {
    constructor(props) {
        super(props);
        this.state = { data: dailyData }
    }

    render() {
        return <Chart data={this.state.data} victory={victory} platform={'web'} />;
    }
}

