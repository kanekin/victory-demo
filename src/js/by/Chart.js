import React from 'react';
import moment from 'moment';
;
export default function Chart(props) {
    const { data } = props;
    const { VictoryBar, VictoryChart, VictoryAxis, VictoryTooltip, VictoryVoronoiContainer } = props.victory;
    const processedData = data.value.map(value => {
        return {
            x: new Date(value.startDate).getTime(),
            y: value.value,
            label: `${moment(value.startDate).format('ddd, Do MMM')}\n${Math.floor(value.value)} steps`
        };
    });
    const tickValues = processedData.filter(datum => moment(datum.x).day() === 0).map(datum => datum.x);
    return React.createElement(VictoryChart, { domainPadding: 10, containerComponent: React.createElement(VictoryVoronoiContainer, { dimension: "x" }) },
        React.createElement(VictoryAxis, { tickFormat: date => moment(date).format('l'), tickValues: tickValues, style: { grid: { stroke: 'white' } } }),
        React.createElement(VictoryAxis, { dependentAxis: true, style: { grid: { stroke: 'white' } } }),
        React.createElement(VictoryBar, { data: processedData, labelComponent: React.createElement(VictoryTooltip, null) }));
}
//# sourceMappingURL=Chart.js.map