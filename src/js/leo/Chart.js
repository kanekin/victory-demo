import React from 'react';
import moment from 'moment';
// interface DataShape {
// };
export default function Chart(props) {
    const { data } = props;
    const { VictoryBar, VictoryChart, VictoryAxis, VictoryTooltip, VictoryLine, VictoryLabel, VictoryArea, VictoryZoomContainer } = props.victory;
    const styles = helper.getStyles();
    const TOTAL_DAYS = 35, PRESSURE_MIN = 40, PRESSURE_MAX = 200;
    let i, xTickValues = [], xTickLabels = [], yTickValues = [], yTickLabels = [];
    for (i = 0; i < TOTAL_DAYS; i++) {
        xTickValues.push(i);
        xTickLabels.push(i); // ["1\nJan","2", "3", "4", "1\nFeb", "2", "3", "4"]
    }
    for (i = PRESSURE_MIN; i < PRESSURE_MAX; i++) {
        yTickValues.push(i);
        yTickLabels.push(i); // ["1\nJan","2", "3", "4", "1\nFeb", "2", "3", "4"]
    }
    const START_DATE = new Date(2017, 0, 1);
    const END_DATE = new Date(2017, 11, 31);
    const START_DATE_MS = START_DATE.valueOf();
    const MS_IN_DAY = 86400 * 1000;
    let item, date, sysSet = [], diaSet = [];
    for (i = 0; i < data.length; i++) {
        item = data[i];
        date = new Date(START_DATE_MS + i * MS_IN_DAY);
        sysSet.push({ x: date, y: item.sys });
        diaSet.push({ x: date, y: item.dia });
    }
    // {"monthName":"Jan","month":1,"yearDay":0,"dia":65,"sys":122}
    // console.log("START_DATE: " + START_DATE);
    // console.log("END_DATE: " + END_DATE);
    const areaDomain = { x: [START_DATE, END_DATE], y: [40, 200] };
    return (React.createElement(VictoryChart, { scale: { x: "time", y: "linear" }, containerComponent: React.createElement(VictoryZoomContainer, { zoomDomain: { x: [START_DATE, END_DATE], y: [0, 200] }, minimumZoom: { x: 1, y: 1 }, dimension: "x" }) },
        React.createElement(VictoryLabel, { x: 25, y: 24, style: {
                textAnchor: "start",
                verticalAnchor: "end",
                fill: "#b40f19",
                fontFamily: "inherit",
                fontSize: "13px",
                fontWeight: "bold"
            }, text: "Blood pressure" }),
        React.createElement(VictoryArea
        // containerComponent={<VictoryVoronoiContainer labels={(d) => `${round(d.x, 2)}, ${round(d.y, 2)}`}/>}
        , { 
            // containerComponent={<VictoryVoronoiContainer labels={(d) => `${round(d.x, 2)}, ${round(d.y, 2)}`}/>}
            labels: (d) => `y: ${d.y}`, labelComponent: React.createElement(VictoryTooltip, { y: 200 }), scale: { x: "time", y: "linear" }, domain: areaDomain, data: sysSet, interpolation: "monotoneX", style: {
                data: {
                    fill: "#777fd7", fillOpacity: 0.25, stroke: "#dd777b", strokeWidth: 1.1
                },
                labels: {
                    fontSize: 11, fill: "#c43a31"
                }
            } }),
        React.createElement(VictoryArea, { scale: { x: "time", y: "linear" }, domain: areaDomain, data: diaSet, style: {
                data: {
                    fill: "#fafafa", fillOpacity: 1, stroke: "#aec0f1", strokeWidth: 1.1
                },
                labels: {
                    fontSize: 11, fill: "#c43a31"
                }
            } }),
        React.createElement(VictoryLine, { scale: { x: "time", y: "linear" }, data: [
                { x: START_DATE, y: 80, y0: 0 },
                { x: END_DATE, y: 80, y0: 0 }
            ], interpolation: "monotoneX", style: {
                data: { stroke: "#0000bd", strokeWidth: 1.5 },
                tickLabels: {
                    fill: "blue",
                    fontFamily: "inherit",
                    fontSize: 11
                }
            } }),
        React.createElement(VictoryLine, { scale: { x: "time", y: "linear" }, data: [
                { x: START_DATE, y: 120, y0: 0 },
                { x: END_DATE, y: 120, y0: 0 }
            ], interpolation: "monotoneX", style: {
                data: { stroke: "#c50000", strokeWidth: 1.5 },
                tickLabels: {
                    fill: "blue",
                    fontFamily: "inherit",
                    fontSize: 11
                }
            } })));
}
const helper = {
    getY: (hour, minute) => {
        if (hour < 12) {
            const delta = 12 - (hour + ((minute / 60) * 1));
            return 12 + delta;
        }
        else {
            const delta = 24 - (hour + ((minute / 60) * 1));
            return 24 + delta;
        }
    },
    parseHKSleepData: (sleepData) => {
        return sleepData.map(s => {
            const startDate = moment(s.startDate);
            const endDate = moment(s.endDate);
            const anchor = (startDate.hour() < 12) ? startDate.subtract(1, 'days') : startDate;
            return {
                date: {
                    year: anchor.year(),
                    month: anchor.month(),
                    date: anchor.date()
                },
                start: {
                    hour: startDate.hour(),
                    minute: startDate.minute()
                },
                end: {
                    hour: endDate.hour(),
                    minute: endDate.minute()
                }
            };
        });
    },
    zeroPad: (num, places) => {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    },
    getTickValues: () => {
        return [
            new Date(2017, 5, 13),
            new Date(2017, 5, 20),
            new Date(2017, 5, 25),
            new Date(2017, 5, 30),
        ];
    },
    getStyles() {
        return {
            // INDEPENDENT AXIS
            axisX: {
                axis: { stroke: "black", strokeWidth: 1 },
                ticks: {
                    // size: (tick) => {
                    //     let tickSize =tick;
                    //     return tickSize;
                    // },
                    stroke: "black",
                    strokeWidth: 1
                },
                tickLabels: {
                    fill: "black",
                    fontFamily: "inherit",
                    fontSize: 16
                }
            },
        };
    }
};
//# sourceMappingURL=Chart.js.map