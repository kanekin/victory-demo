import React from "react";
import * as victory from "victory";
import {BLOOD_PRESSURE_DATA} from "./hardcoded_data"
import Chart from './Chart'

// import moment from 'moment-es6';


export class LeoChart extends React.Component {
    // const BLUE_COLOR = "#00a3de";
    // const RED_COLOR = "#ff00ff";

  constructor(props) {
    super(props);
    this.state = { data: BLOOD_PRESSURE_DATA }
  }

  render() {
    return <Chart data={this.state.data} victory={victory} platform={'web'} />;
  }

}

