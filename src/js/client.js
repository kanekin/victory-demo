import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { VictoryBar, VictoryChart, VictoryAxis, VictoryTheme } from 'victory';
import data from '../data/step_count_distribution.js';
// import custom_chart from './custom_chart.js';


class Main extends React.Component {

  format(data) {
    return data.map((element) => {
      element.dateString = moment(element.startDate).format("HH:mm");
      return element;
    });
  }

  render() {
    return (
      <div>
        <VictoryChart
          theme={VictoryTheme.material}
          width={1000}
          height={300}
          >
          <VictoryAxis
            // tickValues specifies both the number of ticks and where
            // they are placed on the axis
            tickValues={['00:00', '06:00', '12:00', '18:00', '23:50']}
          />
          <VictoryAxis
            dependentAxis
          />
          <VictoryBar
            style={{
              data: {fill: "tomato"}
            }}
            data={this.format(data)}
            x="dateString"
            y="value"
          />
        </VictoryChart>
      </div>
    );
  }
}

const app = document.getElementById('app');
ReactDOM.render(<Main />, app);