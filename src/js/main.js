/**
 * Created by leo on 14/07/2017.
 */

import { LeoChart } from './leo/leo_chart';
import { BYKimChart } from './by/bykim_chart';

import React from 'react';
import ReactDOM from 'react-dom';
import {TatsuyaChart} from "./tatsuya/tatsuya_chart";


export class Main extends React.Component {
    render() {
        const styles = this.getStyles();

        return (
            <g>
                 <svg style={styles.style_bykim} viewBox="0 0 450 350">
                    <rect x="0" y="0" width="10" height="30" fill="#f01616"/>
                    <rect x="420" y="10" width="20" height="20" fill="#458ca8"/>
                    <BYKimChart />
                </svg> 

                <svg style={styles.style_tatsuya} viewBox="0 0 450 350">
                    <rect x="0" y="0" width="10" height="30" fill="#f01616"/>
                    <rect x="420" y="10" width="20" height="20" fill="#458ca8"/>
                    <TatsuyaChart />
                </svg>

                <svg style={styles.style_leo} viewBox="0 0 450 350">
                    {/*<rect x="0" y="0" width="10" height="30" fill="#f01616"/>*/}
                    {/*<rect x="420" y="10" width="20" height="20" fill="#458ca8"/>*/}
                    <LeoChart />
                </svg>
            </g>
        )
    }

    getStyles() {
        const padding = 45;
        const marginBottom = 40;

        return {
            style_bykim: {
                background: "#ccdee8",
                boxSizing: "border-box",
                display: "inline",
                padding: padding,
                marginBottom: marginBottom,
                fontFamily: "'Fira Sans', sans-serif",
                width: "90%",
                height: "auto"
            },

            style_leo: {
                background: "#fafafa",
                boxSizing: "border-box",
                display: "inline",
                padding: padding,
                marginBottom: marginBottom,
                fontFamily: "'Fira Sans', sans-serif",
                width: "90%",
                height: "auto"
            },
            style_tatsuya: {
                background: "#1E1842",
                boxSizing: "border-box",
                display: "inline",
                padding: padding,
                marginBottom: marginBottom,
                fontFamily: "'Fira Sans', sans-serif",
                width: "90%",
                height: "auto"
            }
        };
    }
    }


ReactDOM.render(<Main/>, app);